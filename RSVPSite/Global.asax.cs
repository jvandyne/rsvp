﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Cts.HR.Recruiting.RSVP.Models;

namespace Cts.HR.Recruiting.RSVP
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
           ModelBinders.Binders.Add(typeof(DateTime), new Models.DateTimeModelBinder());

           Database.SetInitializer(new CreateDatabaseIfNotExists<RSVPDBContext>());

           using (RSVPDBContext ctx = new RSVPDBContext())
           {
               ctx.Database.Initialize(true);
           }
           
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            
        }

       ////authorization on every controller(except log in)
       // public class FilterConfig
       // {
       //    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
       //    {
       //       filters.Add(new HandleErrorAttribute());
       //       filters.Add(new System.Web.Mvc.AuthorizeAttribute());
       //    }
       // }
    }
}