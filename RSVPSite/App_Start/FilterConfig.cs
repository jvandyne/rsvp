﻿using System.Web;
using System.Web.Mvc;

namespace Cts.HR.Recruiting.RSVP
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}