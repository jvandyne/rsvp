﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using Cts.HR.Recruiting.RSVP.Models;
using Cts.HR.Recruiting.RSVP.ViewModels;
using System.Web.Configuration;

namespace Cts.HR.Recruiting.RSVP.Controllers
{
   [Authorize]
    public class AttendeeController : Controller
    {
        private RSVPDBContext db = new RSVPDBContext();

        //
        // GET: /Attendee/

        public ActionResult Index(int? id)
        {
           //get event id list attendees
           var m = db.Events.Where(e => e.Id == id).Include(a => a.Attendees).FirstOrDefault();
           List<Attendee> model = new List<Attendee>(m.Attendees);
            return View(model);
        }

        //
        // GET: /Attendee/Excel/2

        //public ExcelResult Excel(int? id)
        public ActionResult Excel(int? id)
        {
            var attendees = db.Attendee.Where(a => a.EventId == id)
                .ToList()
                .Select(attendee => string.Format("{0},{1},{2},{3}", attendee.FirstName, attendee.LastName, attendee.Email, attendee.Phone));

            var csvString = string.Join(Environment.NewLine, attendees.ToArray());
            var bytes = System.Text.Encoding.ASCII.GetBytes(csvString);
            return File(bytes, "application/xls", db.Events.Single(a => a.Id == id).Name + " - Attendees.csv");

           }


        //
        // GET: /Attendee/Details/5

        public ActionResult Details(int id = 0)
        {
           //TODO: fix back to list action link on view
           //TODO: fix layout of view
           
           //pass attendee with event to display custom questions
           //RegisterData rd = new RegisterData
           //{
           //   Attendee = db.Attendee.Include(e => e.Answer).Include(e => e.Event).SingleOrDefault(a => a.Id == id)
           //};




           Attendee Attendee = db.Attendee.Include(e => e.Answer).Include(e => e.Event).SingleOrDefault(a => a.Id == id);
            if (Attendee == null)
            {
                return HttpNotFound();
            }
            return View(Attendee);
        }

        //register for event
        // GET: /Attendee/Create
       [AllowAnonymous]
        public ActionResult Create(int id = 0)
        {
          //displaytemplate
           var regData = new RegisterData
           {
              //Event = db.Events.Include(e => e.CustomQuestions).SingleOrDefault(e => e.Id == id)
              Event = db.Events.Find(id)
           };
            return View(regData);
        }

        //
        // POST: /Attendee/Create

        [HttpPost]
       [AllowAnonymous]
        public ActionResult Create(RegisterData regData)
       {          
          string ukey = RandomKey();
            regData.Attendee.UserKey = ukey;

            regData.Attendee.EventId = regData.Event.Id;
            var errors = ModelState.Values.Where(a => a.Errors.Any());
            if (ModelState.IsValid)
            {
               //regData.Responses.ForEach(r => regData.Attendee.Answers.Add(new Answer { reply = r }));
               db.Attendee.Add(regData.Attendee);                             
               db.SaveChanges();              
            }

            #region email format
            //what then email is supposed to look like
            /*Hello <firstname>,
 
             Thank you for registering for the <eventname> on <eventdate>.
 
             <Custom text> (I would like to be able to put this custome text in when creating the event).
 
             We look forward to seeing you there!
 
             CTS */
            #endregion

           //TODO: send confirmation email
            MailMessage msg = new MailMessage();
            msg.To.Add(regData.Attendee.Email);
            msg.Subject = "CTS - Registration Completed";
            msg.From = new MailAddress(WebConfigurationManager.AppSettings["SmtpSender"].ToString());
            msg.IsBodyHtml = true;
            msg.Body = "<div style='font-face: Segoe UI'>Hello " + regData.Attendee.FirstName + ",<br/><br/>" +
             "Thank you for registering for the " + regData.Event.Name + " on " + regData.Event.EventDate.ToString("MM/dd/yyyy") + " at " + regData.Event.EventDate.ToString("h:mm tt") + "<br/><br/>" +
             regData.Event.Texts + "<br/><br/>" +
             "We look forward to seeing you there! <br/><br/>" +
             "CTS</div>";

            SmtpClient smtp = new SmtpClient("PRDCASBHM01.askcts.com");
            smtp.UseDefaultCredentials = false;
            smtp.Send(msg);

            return View("Confirm", regData);
        }   

        //
        // GET: /Attendee/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Attendee Attendee = db.Attendee.Find(id);
            if (Attendee == null)
            {
                return HttpNotFound();
            }
            return View(Attendee);
        }

        //
        // POST: /Attendee/Edit/5

        [HttpPost]
        public ActionResult Edit(Attendee Attendee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Attendee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Attendee);
        }

        //
        // GET: /Attendee/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Attendee Attendee = db.Attendee.Find(id);
            if (Attendee == null)
            {
                return HttpNotFound();
            }
            return View(Attendee);
        }

        //
        // POST: /Attendee/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int? id)
        {
            Attendee Attendee = db.Attendee.Find(id);
            db.Attendee.Remove(Attendee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}
   //Random key to generate the key (this could be changed to something more cryptic 
    
       //
       //GET: /Attendee/Confirm
        public ActionResult Confirm(int? id)
        {
            Attendee attendees = db.Attendee.Find(id);
   
            return View(attendees);
        }

        public string RandomKey()
        {
            var chars = "ABCEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();

            var UserKey = new string(
                Enumerable.Repeat(chars, 10)
                .Select(s => s[random.Next(s.Length)])
                .ToArray());
            return UserKey;
        }
    }
}