﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Cts.HR.Recruiting.RSVP.Models;
using Cts.HR.Recruiting.RSVP.ViewModels;
using Cts.HR.Recruiting.RSVP.Filters;

namespace Cts.HR.Recruiting.RSVP.Controllers
{
   [InitializeSimpleMembership]
   [Authorize]
    public class EventController : Controller
    {
        private RSVPDBContext db = new RSVPDBContext();

        //
        // GET: /Event/

        public ActionResult Index()
        {
           //var model = db.Events.Select(e => new ViewModels.EventData { Event = e })
           //    .ToList();

           //var viewModel = db.Events.Include(e => e.EventType).Select(a => new ViewModels.EventData {Event = a, EventTypes = db.EventTypes, UserProfile = a.Admin}).ToList();
           //viewModel.OrderByDescending(i => i.Event.EventDate);

           var model = db.Events.Include(e => e.EventType).Include(a => a.Admin).ToList();

            return View(model);
        }

        //
        // GET: /Event/Details/5

       [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
           Event Event = db.Events.Include(t => t.EventType).FirstOrDefault(e => e.Id == id);
            if (Event == null)
            {
                return HttpNotFound();
            }
            return View(Event);
        }

        //
        // GET: /Event/Create

        public ActionResult Create()
        {
           //TODO: input validations

           // MembershipUser user = Membership.GetUser(User.Identity.Name);
           //UserProfile admin = db.UserProfiles.Find(user.ProviderUserKey);
           var viewModel = new ViewModels.EventData
           {
              EventTypes = db.EventTypes
              //Event = new Event { Admin = admin }
           };
        
            return View(viewModel);
         }

        //
        // POST: /Event/Create

        /// <summary>Reminder - explanation of model types used here. viewmodel, which contains multiple models, is passed from the view.
        /// if data passed from view doesn't match the model (like data types), ModelState.IsValid will return false.
        /// at db.SaveChanges() spit error when eventtypeid was not passed to EventData.Event, because DDL was used instead of DDLFor 
        /// </summary>
        /// <requirements>must pass required data specified in model</requirements>
        /// <author>Yosuke</author>
        /// <date>4/9/2013</date>
        /// <exception></exception>
        /// <return></return>
        /// <param name="eventData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(ViewModels.EventData eventData)
        {
            MembershipUser user = Membership.GetUser(User.Identity.Name);
            UserProfile admin = db.UserProfiles.Find(user.ProviderUserKey);
            int adminId = admin.UserId;
            eventData.Event.AdminId = adminId;

            //eventData.Event.AdminId = eventData.Event.Admin.UserId;

            var errors = ModelState.Values.Where(a => a.Errors.Any());

            if (ModelState.IsValid)
            {
                //eventData.CustomQuestions.ForEach(q => eventData.Event.CustomQuestions.Add(new CustomQuestion{Question=q}));
                //eventData.Event.CustomQuestions.add

                //get eventdata.event just inserted from the db, use the pk of that store the q into db

                #region alternate method for storing questions -yosuke
                //foreach (string q in eventData.CustomQuestions)
                //{
                //   if (string.IsNullOrWhiteSpace(q) == false)
                //   {
                //      eventData.Question.Question = q;
                //      eventData.Question.EventID = id;
                //      db.Questions.Add(eventData.Question);
                //   }
                //}
                #endregion


                #region questions (initial attempt) yosuke
                //if (string.IsNullOrWhiteSpace(eventData.Question.Question) == false)
                //{
                //   db.Questions.Add(eventData.Question);

                //   db.SaveChanges();//saving data causes ID creation, which is used as FK when inserting custom quesitons below

                //   foreach (string q in eventData.CustomQuestions)//pulling questions 2-5 which stored into list 
                //   {
                //      if (string.IsNullOrWhiteSpace(q) == false)
                //      {
                //         eventData.Question.Question = q;
                //         db.Questions.Add(eventData.Question);//eventData.Question already contains PK, allowing new custom question to be inserted here
                //         db.SaveChanges();
                //      }
                //   }
                //}
                //else if (eventData.CustomQuestions.Count > 0)
                //{
                //   db.SaveChanges();
                //   int id = eventData.Event.Id;
                //   foreach (string q in eventData.CustomQuestions)
                //   {
                //      if (string.IsNullOrWhiteSpace(q) == false)
                //      {
                //         eventData.Question.EventID = id;
                //         eventData.Question.Question = q;
                //         db.Questions.Add(eventData.Question);//eventData.Question already contains PK, allowing new custom question to be inserted here
                //         db.SaveChanges();
                //      }
                //   }
                //}
                #endregion

                db.Events.Add(eventData.Event);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(); //TODO: model is not valid, do exception handling here. display error message on create?
        }

        //
        // GET: /Event/Edit/5

        public ActionResult Edit(int id = 0)
        {
           #region edit question scott
           //Event Event = db.Events.Find(id);
           
            //    CustomQuestion Question = db.Questions.Find(id);
       
            //if ((Event == null) || (Question == null)) 
            //{
            //    return HttpNotFound();
            //}     
            
            
            //this.ViewBag.CustomQuestionID = new SelectList(this.db.Questions, "CustomQuestionID", "Question", Question.CustomQuestionID);
           //this.ViewBag.Id = new SelectList(this.db.Events, "Id", "Name", Event.Id);
           #endregion

           //var viewModel = new ViewModels.EditEventData
           //{
           //   //Event = db.Events.Find(id),
           //   Event = db.Events.Include( e => e.CustomQuestions).SingleOrDefault( e => e.Id == id),
           //   EventTypes = db.EventTypes
           //};  
           //Event model = new Event();
           //model = db.Events.Find(id);

           var viewModel = new ViewModels.EditEventData
           {
              Event = db.Events.Find(id),
              EventTypes = db.EventTypes
           };
           
           return View(viewModel);
        }

        #region edit questions scott
        //added by scott 4-20 testing views/edits
        //public ActionResult EditQuestion(int id = 0)
        //{
         
        //    CustomQuestion Question = db.Questions.Find(id);

        //  if (Question == null)
        //    {
        //        return HttpNotFound();
        //    }

        //                this.ViewBag.CustomQuestionID = new SelectList(this.db.Questions, "CustomQuestionID", "Question", Question.CustomQuestionID);

        //                return View();
        //}
        #endregion
        //
        // POST: /Event/Edit/5

        [HttpPost]
        public ActionResult Edit(EditEventData postData)
        {
           MembershipUser user = Membership.GetUser(User.Identity.Name);
           UserProfile admin = db.UserProfiles.Find(user.ProviderUserKey);
           int adminId = admin.UserId;
           postData.Event.AdminId = adminId;

           var errors = ModelState.Values.Where(a => a.Errors.Any());

           //TODO: try foreach to remove customquestions from db then re-enter customquestion values of event 

           //foreach (CustomQuestion q in postData.Event.CustomQuestions)
           //{
           //   int id = q.CustomQuestionID;
           //   CustomQuestion cq = db.Questions.Find(id);
           //   db.Questions.Remove(cq);
           //   db.Questions.Add(q);              
           //}
           db.SaveChanges();

           if (ModelState.IsValid)
           {
              db.Entry(postData.Event).State = EntityState.Modified;
              db.SaveChanges();
              return RedirectToAction("Index");
           }
 
         return View(postData); //TODO: display error
        }

        //
        // GET: /Event/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Event Event = db.Events.Find(id);
            if (Event == null)
            {
                return HttpNotFound();
            }
            return View(Event);
        }

        //
        // POST: /Event/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Event Event = db.Events.Find(id);
            db.Events.Remove(Event);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}

//*******************************************************************************
//Code from the EventCreateController using --> public ActionResult Whatever(string events)
//It uses a 'string' instead of an 'int' to pull the Event up for Editing, Deleting, Viewing
//*******************************************************************************
// Example:
//       public ActionResult Details(string events)    <---using a 'string'
//    {
//        Event Event = db.Events.Find(events);
//        if (Event == null)
//        {
//            return HttpNotFound();
//        }
//        return View(Event);
//    }
//
//*******************************************************************************
//It's helpful because in the "View"  --> Event --> Index.cshtml 
//You can do the same thing with an 'int'  -- it is going to be  a preference 
//and ease of access/use (like is it easier to get to the 'int' or easier to get to the 'string')
//we can use a @foreach to create a link inde<ul>
//    @foreach (var events in Model)
//    { 
//    <li>@Html.ActionLink(events.EventName, "Details", new {events = events.EventName})</li>
//    }
//</ul>
//
//*******************************************************************************x automatically
//Example: 