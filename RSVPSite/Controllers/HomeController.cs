﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cts.HR.Recruiting.RSVP.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Event Management";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Event RSVP";

            return View();
        }

        public ActionResult Contact()
        {

            ViewBag.Message = "Contact Point";

            return View();
        }
    }
}