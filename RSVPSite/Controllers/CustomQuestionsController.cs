﻿//Using this for testing purposes to check for the edit and details functionality in the Event page
//so that we can edit and delete questions at the same time as the event -Scott 4-2013
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cts.HR.Recruiting.RSVP.Models;

namespace Cts.HR.Recruiting.RSVP.Controllers
{
    public class CustomQuestionsController : Controller
    {
        private RSVPDBContext db = new RSVPDBContext();

        //
        // GET: /CustomQuestions/

        public ActionResult Index()
        {
            var questions = db.Questions.Include(c => c.Event);
            return View(questions.ToList());
        }

        //
        // GET: /CustomQuestions/Details/5

        public ActionResult Details(int id = 0)
        {
            CustomQuestion customquestion = db.Questions.Find(id);
            if (customquestion == null)
            {
                return HttpNotFound();
            }
            return View(customquestion);
        }

        //
        // GET: /CustomQuestions/Create

        public ActionResult Create()
        {
            ViewBag.EventID = new SelectList(db.Events, "Id", "Name");
            return View();
        }

        //
        // POST: /CustomQuestions/Create

        [HttpPost]
        public ActionResult Create(CustomQuestion customquestion)
        {
            if (ModelState.IsValid)
            {
                db.Questions.Add(customquestion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EventID = new SelectList(db.Events, "Id", "Name", customquestion.EventID);
            return View(customquestion);
        }

        //
        // GET: /CustomQuestions/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CustomQuestion customquestion = db.Questions.Find(id);
            if (customquestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.EventID = new SelectList(db.Events, "Id", "Name", customquestion.EventID);
            return View(customquestion);
        }

        //
        // POST: /CustomQuestions/Edit/5

        [HttpPost]
        public ActionResult Edit(CustomQuestion customquestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customquestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EventID = new SelectList(db.Events, "Id", "Name", customquestion.EventID);
            return View(customquestion);
        }

        //
        // GET: /CustomQuestions/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CustomQuestion customquestion = db.Questions.Find(id);
            if (customquestion == null)
            {
                return HttpNotFound();
            }
            return View(customquestion);
        }

        //
        // POST: /CustomQuestions/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            CustomQuestion customquestion = db.Questions.Find(id);
            db.Questions.Remove(customquestion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}