﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cts.HR.Recruiting.RSVP.Models;

namespace Cts.HR.Recruiting.RSVP.Controllers
{
    public class UserController : Controller
    {
        private RSVPDBContext db = new RSVPDBContext();

        //
        // GET: /User/

        public ActionResult Index()
        {
            return View(db.Login.ToList());
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(int id = 0)
        {
            User User = db.Login.Find(id);
            if (User == null)
            {
                return HttpNotFound();
            }
            return View(User);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(User User)
        {
            if (ModelState.IsValid)
            {
                db.Login.Add(User);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(User);
        }

        //
        // GET: /User/Edit/5

        public ActionResult Edit(int id = 0)
        {
            User User = db.Login.Find(id);
            if (User == null)
            {
                return HttpNotFound();
            }
            return View(User);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public ActionResult Edit(User User)
        {
            if (ModelState.IsValid)
            {
                db.Entry(User).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(User);
        }

        //
        // GET: /User/Delete/5

        public ActionResult Delete(int id = 0)
        {
            User User = db.Login.Find(id);
            if (User == null)
            {
                return HttpNotFound();
            }
            return View(User);
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            User User = db.Login.Find(id);
            db.Login.Remove(User);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}