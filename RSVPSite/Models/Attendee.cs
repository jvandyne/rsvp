﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

    namespace Cts.HR.Recruiting.RSVP.Models
    {
            
   

        public class Attendee
        {
           //public Attendee()
           //{
           //   this.Answers = new List<Answer>();
           //}

            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int Id { get; set; }
            public int EventId { get; set; }
            [Required(ErrorMessage = "First Name is required")]
            public string FirstName { get; set; }
            [Required(ErrorMessage = "Last Name is required")]
            public string LastName { get; set; }
            [Required(ErrorMessage = "E-mail is required")]
            [RegularExpression(@"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$", ErrorMessage = "Please enter a valid e-mail address")]
            public string Email { get; set; }
            [RegularExpression(@"^[-0-9]{0,15}$", ErrorMessage = "Phone must contain only numbers")]
            public string Phone { get; set; }
            public string Company { get; set; }
            public string School { get; set; }
            public string Address { get; set; }
            public string WebURL { get; set; }
            public string UserKey { get; set; }

            public virtual Event Event { get; set; }
            public virtual Answer Answer { get; set; }
            //public virtual ICollection<Answer> Answers { get; set; }

        }

    }
