﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cts.HR.Recruiting.RSVP.Models
{
    public class CustomQuestion
    {
       [Key]
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomQuestionID { get; set; }

        public int EventID { get; set; }
        public string Question { get; set; }

        //public virtual Event Event { get; set; }
        //public virtual  ICollection<Answer> Answers { get; set; }

    }
}