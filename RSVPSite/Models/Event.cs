﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cts.HR.Recruiting.RSVP.Models
{
   public class Event
   {
      public Event()
      {
         //this.CustomQuestions = new List<CustomQuestion>();
         this.Attendees = new List<Attendee>();
      }

      [Required]
      public string Name { get; set; }

      [Key]
      [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
      public int Id { get; set; }

      public int AdminId { get; set; }

      [Display(Name = "Date")]
      [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
      [Required]
      public DateTime EventDate { get; set; }

      public int EventTypeId { get; set; }

      [Required]
      public string Location { get; set; }

      public string Address1 { get; set; }

      public string Address2 { get; set; }

      public string City { get; set; }

      public string State { get; set; }

      public string Zip { get; set; }

      [Display(Name = "Email Custom Texts")]
      public string Texts { get; set; }

      public string Question1 { get; set; }
      public string Question2 { get; set; }
      public string Question3 { get; set; }
      public string Question4 { get; set; }
      public string Question5 { get; set; }

      //public virtual ICollection<CustomQuestion> CustomQuestions { get; set; }
      public virtual EventType EventType { get; set; }
      public virtual ICollection<Attendee> Attendees { get; set; }
      public virtual UserProfile Admin { get; set; }
   }
}