﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Cts.HR.Recruiting.RSVP.Models
{
    public class RSVPDBContext : DbContext
    {
        public RSVPDBContext() : base("DefaultConnection")
        {
           base.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Attendee> Attendee { get; set; }
    //    public DbSet<User> Login { get; set; }

        public DbSet<CustomQuestion> Questions { get; set; }
        public DbSet<EventType> EventTypes { get; set; }
    }

    //Migrated them to separate class file I believe it's ok now. -Yosuke
    //This is the AccountModels.cs just pasted in -- it makes it easier if it is all in one place
    //for migrations and altering -Scott
    //[Table("UserProfile")]
    //public class UserProfile
    //{
    //   [Key]
    //   [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
    //   public int UserId { get; set; }
    //   public string UserName { get; set; }
    //   public string FullName { get; set; }
    //   public string OfficeLocation { get; set; }

    //}

    //public class RegisterExternalLoginModel
    //{
    //   [Required]
    //   [Display(Name = "User name")]
    //   public string UserName { get; set; }

    //   public string ExternalLoginData { get; set; }
    //}

    //public class LocalPasswordModel
    //{
    //   [Required]
    //   [DataType(DataType.Password)]
    //   [Display(Name = "Current password")]
    //   public string OldPassword { get; set; }

    //   [Required]
    //   [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
    //   [DataType(DataType.Password)]
    //   [Display(Name = "New password")]
    //   public string NewPassword { get; set; }

    //   [DataType(DataType.Password)]
    //   [Display(Name = "Confirm new password")]
    //   [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
    //   public string ConfirmPassword { get; set; }

    //   //I added them to this model so I could use the partial on the "Manage" page

    //   [Required]
    //   [DataType(DataType.Text)]
    //   [Display(Name = "Full Name")]
    //   public string FullName { get; set; }

    //   [Required]
    //   [DataType(DataType.Text)]
    //   [Display(Name = "Office Location")]
    //   public string OfficeLocation { get; set; }
    //}

    //public class LoginModel
    //{
    //   [Required]
    //   [Display(Name = "User name")]
    //   public string UserName { get; set; }

    //   [Required]
    //   [DataType(DataType.Password)]
    //   [Display(Name = "Password")]
    //   public string Password { get; set; }

    //   [Display(Name = "Remember me?")]
    //   public bool RememberMe { get; set; }
    //}

    //public class RegisterModel
    //{
    //   [Required]
    //   [Display(Name = "User name")]
    //   public string UserName { get; set; }

    //   [Required]
    //   [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
    //   [DataType(DataType.Password)]
    //   [Display(Name = "Password")]
    //   public string Password { get; set; }

    //   [DataType(DataType.Password)]
    //   [Display(Name = "Confirm password")]
    //   [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
    //   public string ConfirmPassword { get; set; }

    //   //these are the fields that are added
    //   [Required]
    //   [DataType(DataType.Text)]
    //   [Display(Name = "Full Name")]
    //   public string FullName { get; set; }

    //   [Required]
    //   [DataType(DataType.Text)]
    //   [Display(Name = "Office Location")]
    //   public string OfficeLocation { get; set; }
    //}


    //public class ExternalLogin
    //{
    //   public string Provider { get; set; }
    //   public string ProviderDisplayName { get; set; }
    //   public string ProviderUserId { get; set; }
    //}
 }