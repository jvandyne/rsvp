﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cts.HR.Recruiting.RSVP.Models;

namespace Cts.HR.Recruiting.RSVP.Models
{
   public class Answer
   {
      [Key]
      public int AttendeeId { get; set; }
      public string Answer1 { get; set; }
      public string Answer2 { get; set; }
      public string Answer3 { get; set; }
      public string Answer4 { get; set; }
      public string Answer5 { get; set; }

   }
}