﻿using ExcelExporter;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cts.HR.Recruiting.RSVP
{
    public class ExcelResult : FilePathResult
    {
        static readonly Dictionary<ExcelFormat, string> ContentTypes = new Dictionary<ExcelFormat, string> {
            { ExcelFormat.Excel2003,	"application/vnd.ms-excel" },
            { ExcelFormat.Excel2007,	"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" },
            { ExcelFormat.Excel2007Binary,	"application/vnd.ms-excel.sheet.binary.macroEnabled.12" },
            { ExcelFormat.Excel2007Macro,	"application/vnd.ms-excel.sheet.macroEnabled.12" }
        };

        static string GetFileName(string extension)
        {
            var path = Path.GetTempFileName();
            File.Delete(path);
            return Path.ChangeExtension(path, extension);
        }
        
        public ExcelFormat Format { get; private set; }

        
        public ExcelResult(string name, ExcelFormat format)	
            : base(GetFileName(ExcelExport.GetExtension(format)), ContentTypes[format])
        {
            FileDownloadName = name;
            Format = format;
        }

        readonly ExcelExport exporter = new ExcelExport();

        
        public ExcelExport AddSheet<TRow>(string sheetName, IEnumerable<TRow> items)
        {
            return exporter.AddSheet(sheetName, items);
        }

        public ExcelExport AddSheet(DataTable table)
        {
            if (table == null) throw new ArgumentNullException("table");
            return AddSheet(table.TableName, table);
        }
        public ExcelExport AddSheet(string sheetName, DataTable table)
        {
            return exporter.AddSheet(sheetName, table);
        }

        public override void ExecuteResult(ControllerContext context)
        {
            exporter.ExportTo(FileName, Format);
            base.ExecuteResult(context);
        }
    }
}
