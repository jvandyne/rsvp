﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Cts.HR.Recruiting.RSVP.Models
{
    public class EventType
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}