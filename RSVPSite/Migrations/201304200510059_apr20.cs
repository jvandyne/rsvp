namespace Cts.HR.Recruiting.RSVP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class apr20 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Events", "EventTypeId", "dbo.EventTypes");
            DropIndex("dbo.Events", new[] { "EventTypeId" });
            AddColumn("dbo.Events", "AdminId", c => c.Int(nullable: false));
            AddColumn("dbo.Events", "Admin_UserId", c => c.Int());
            AlterColumn("dbo.Events", "EventTypeId", c => c.Int(nullable: false));
            AddForeignKey("dbo.Events", "EventTypeId", "dbo.EventTypes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Events", "Admin_UserId", "dbo.UserProfile", "UserId");
            CreateIndex("dbo.Events", "EventTypeId");
            CreateIndex("dbo.Events", "Admin_UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Events", new[] { "Admin_UserId" });
            DropIndex("dbo.Events", new[] { "EventTypeId" });
            DropForeignKey("dbo.Events", "Admin_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Events", "EventTypeId", "dbo.EventTypes");
            AlterColumn("dbo.Events", "EventTypeId", c => c.Int());
            DropColumn("dbo.Events", "Admin_UserId");
            DropColumn("dbo.Events", "AdminId");
            CreateIndex("dbo.Events", "EventTypeId");
            AddForeignKey("dbo.Events", "EventTypeId", "dbo.EventTypes", "Id");
        }
    }
}
