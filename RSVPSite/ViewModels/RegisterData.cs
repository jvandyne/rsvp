﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cts.HR.Recruiting.RSVP.Models;

namespace Cts.HR.Recruiting.RSVP.ViewModels
{
   public class RegisterData
   {
      public Event Event { get; set; }
      public Attendee Attendee { get; set; }
      //public CustomQuestion Questions { get; set; }
      //public List<string> Responses { get; set; }
   }
}