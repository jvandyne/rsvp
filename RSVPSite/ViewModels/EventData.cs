﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cts.HR.Recruiting.RSVP.Models;

namespace Cts.HR.Recruiting.RSVP.ViewModels
{
    public class EventData
    {
        public Event Event { get; set; }
        //public CustomQuestion Question { get; set; }
        public IEnumerable<EventType> EventTypes { get; set; }
        public UserProfile UserProfile { get; set; }
        //public List<string> CustomQuestions { get; set; }
        //public List<CustomQuestion> QuesList { get; set; }
        //public IEnumerable<Event> Events { get; set; }
        
        //public IEnumerable<EventType> EventTypes { get; set; }
    }
}