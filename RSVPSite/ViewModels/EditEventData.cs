﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cts.HR.Recruiting.RSVP.Models;

namespace Cts.HR.Recruiting.RSVP.ViewModels
{
   public class EditEventData
   {
      public Event Event { get; set; }
      //public CustomQuestion Question { get; set; }
      public IEnumerable<EventType> EventTypes { get; set; }
   }
}